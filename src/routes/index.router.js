const { Router } = require('express');
const router = Router();

router.get('/', (req, resp) => {
    resp.send('Hello from docker')
});

module.exports = router