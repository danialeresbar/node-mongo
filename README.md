# NodeJS-MongoDB app

A simple practical example of how to create a Node and MongoDB 
project using Docker containers. For this, we will use Docker Compose, 
which will allow us to connect different containers in a simple way.

## NodeJS

![nodejs](https://upload.wikimedia.org/wikipedia/commons/d/d9/Node.js_logo.svg)

[Node.js](https://nodejs.org/en/) is an open-source, cross-platform, 
back-end JavaScript runtime environment that runs on the V8 engine and 
executes JavaScript code outside a web browser.

## MongoDB

![mongoDB](https://infinapps.com/wp-content/uploads/2018/10/mongodb-logo.png)

[MongoDB](https://www.mongodb.com/) is a source-available cross-platform 
document-oriented database program. Classified as a NoSQL database program, 
MongoDB uses JSON-like documents with optional schemas. MongoDB is developed 
by MongoDB Inc. and licensed under the Server Side Public License.